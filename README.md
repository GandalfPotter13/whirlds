You can play any .utw file in Mars Explorer, og Syn3h etc using the **raw** URL, just **replace https:// with http://** because Mars Explorer does not support https! Example: `http://gitea.moe/lamp/whirlds/raw/branch/master/marsxplr/Freestyle/Whirld.utw`

More worlds here: https://gitea.moe/lamp/map.syn3h.com-mirror/src/branch/modified/map.syn3h.com.s3.amazonaws.com